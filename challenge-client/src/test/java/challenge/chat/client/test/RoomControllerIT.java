package challenge.chat.client.test;

import challenge.chat.dto.request.CreateRoomRequestTransport;
import challenge.chat.dto.response.ChatRoomResponseTransport;
import challenge.chat.dto.response.ResponseMessageTransport;
import challenge.chat.util.ChatAppException;
import challenge.chat.util.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.annotation.Order;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class RoomControllerIT {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() {
        RequestHandler.setToken("9");
    }

    @After
    public void deleteSetup() {
        RequestHandler.setToken("");
    }

    @Test
    public void addUserToRoom_withRoomId_addsUserToGivenChatRoom() {
        joinRoom();
    }

    @Test(expected = ChatAppException.class)
    public void addUserToRoom_withRoomId_throwsRoomNotFoundError() {
        try {
            int id = 0;
            Map<String, String> params = new Hashtable<>();
            params.put("id", id + "");
            String response = RequestHandler.sendJsonAndParameterPostRequest("http://localhost:8080/room/{id}/join", params);
            ChatRoomResponseTransport roomDTO = objectMapper.readValue(response, ChatRoomResponseTransport.class);
            assertNotNull(roomDTO);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    @Test
    public void getRoomOfUser_withNoParameters_returnsUsersRoom() {
        joinRoom();
        String response = RequestHandler.sendGetRequest("http://localhost:8080/room/user");
        assertNotNull(response);
    }

    @Test(expected = ChatAppException.class)
    public void getRoomOfUser_withNoParameters_throwsNoUserFoundError() {
        RequestHandler.setToken("");
        String response = RequestHandler.sendGetRequest("http://localhost:8080/room/user");
        assertNotNull(response);
    }

    @Test
    public void createRoom_withCreateRoomRequestTransport_createsRoom() {
        CreateRoomRequestTransport createRoomRequestTransport = new CreateRoomRequestTransport("test");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(createRoomRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/room/", json);
            ResponseMessageTransport responseMessageTransport = objectMapper.readValue(response, ResponseMessageTransport.class);
            assertEquals("Chat room was created successfully!", responseMessageTransport.getResponseMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = ChatAppException.class)
    public void createRoom_withCreateRoomRequestTransport_throwsNoGivenRoomNameError() {
        CreateRoomRequestTransport createRoomRequestTransport = new CreateRoomRequestTransport("");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(createRoomRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/room/", json);
            assertEquals("Chat room was created successfully!", response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getListOfRooms_withNoParameters_returnsListOfRooms() {
        String response = RequestHandler.sendGetRequest("http://localhost:8080/room/");
        assertNotNull(response);
    }

    @Test(expected = ChatAppException.class)
    public void getListOfRooms_withNoParameters_throwsNoUserFoundError() {
        RequestHandler.setToken("");
        String response = RequestHandler.sendGetRequest("http://localhost:8080/room/");
        assertNotNull(response);
    }

    @Test
    public void removeUserFromRoom_withRoomId_removesUserFromRoom() {
        try {
            int id = 7;
            Map<String, String> params = new Hashtable<>();
            params.put("id", id + "");
            String response = RequestHandler.sendJsonAndParameterPostRequest("http://localhost:8080/room/{id}/leave", params);
            ResponseMessageTransport responseMessageTransport = objectMapper.readValue(response, ResponseMessageTransport.class);
            assertEquals("Left room with id " + id, responseMessageTransport.getResponseMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = ChatAppException.class)
    public void removeUserFromRoom_withRoomId_throwsRoomNotFoundError() {
        int id = 0;
        Map<String, String> params = new Hashtable<>();
        params.put("id", id + "");
        String response = RequestHandler.sendJsonAndParameterPostRequest("http://localhost:8080/room/{id}/leave", params);
        assertNotNull(response);
    }

    public void joinRoom(){
        try {
            int id = 7;
            Map<String, String> params = new Hashtable<>();
            params.put("id", id + "");
            String response = RequestHandler.sendJsonAndParameterPostRequest("http://localhost:8080/room/{id}/join", params);
            ChatRoomResponseTransport roomDTO = objectMapper.readValue(response, ChatRoomResponseTransport.class);
            assertNotNull(roomDTO);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
