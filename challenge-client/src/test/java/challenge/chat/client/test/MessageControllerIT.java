package challenge.chat.client.test;

import challenge.chat.dto.request.UserSendMessageRequestTransport;
import challenge.chat.dto.response.MessageResponseTransport;
import challenge.chat.dto.response.ResponseMessageTransport;
import challenge.chat.dto.response.UserGetNewMessagesResponseTransport;
import challenge.chat.util.ChatAppException;
import challenge.chat.util.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MessageControllerIT {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup(){
        RequestHandler.setToken("9");
    }

    @After
    public void deleteSetup(){
        RequestHandler.setToken("");
    }

    @Test
    public void sendMessage_withMessageRequestTransport_sendsMessageToRoom() {
        UserSendMessageRequestTransport sendMessage = new UserSendMessageRequestTransport("test");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(sendMessage);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/message/", json);
            ResponseMessageTransport responseMessageTransport = objectMapper.readValue(response, ResponseMessageTransport.class);
            assertEquals("Message was sent successfully!", responseMessageTransport.getResponseMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = ChatAppException.class)
    public void sendMessage_withMessageRequestTransport_throwsUserNotFoundError() {
        RequestHandler.setToken("");
        UserSendMessageRequestTransport sendMessage = new UserSendMessageRequestTransport("test");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(sendMessage);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/message/", json);
            assertEquals("Message was sent successfully!", response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getMessages_withLastRequestTime_returnsUnreadMessages(){
        long lastRequestTime = System.currentTimeMillis();
        ResponseEntity<UserGetNewMessagesResponseTransport> responseMessages = RequestHandler.sendGetRequestForMessages();
        assertEquals(HttpStatus.OK, responseMessages.getStatusCode());
    }

    @Test(expected = ChatAppException.class)
    public void getMessages_withLastRequestTime_throwsUserNotFoundError(){
        RequestHandler.setToken("");
        ResponseEntity<UserGetNewMessagesResponseTransport> responseMessages = RequestHandler.sendGetRequestForMessages();
        assertEquals(HttpStatus.FORBIDDEN, responseMessages.getStatusCode());
    }
}
