package challenge.chat.client.test;

import challenge.chat.dto.request.UserRequestTransport;
import challenge.chat.dto.response.ResponseMessageTransport;
import challenge.chat.util.ChatAppException;
import challenge.chat.util.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationControllerIT {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void registerUser_withUsernameAndPassword_registersUser() {
        UserRequestTransport userRequestTransport = new UserRequestTransport("Test", "Test");
        String json;
        try {
            json = objectMapper.writeValueAsString(userRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/authenticate/", json);
            ResponseMessageTransport responseMessageTransport = objectMapper.readValue(response, ResponseMessageTransport.class);
            assertEquals("user-registered!", responseMessageTransport.getResponseMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(expected = ChatAppException.class)
    public void registerUser_withUsernameAndPassword_throwsUserParametersNotFoundError() {
        UserRequestTransport userRequestTransport = new UserRequestTransport(null, "Test");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(userRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/authenticate/", json);
            assertEquals("user-registered!", response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginUser_withUserRequestTransport_loginsUserAndReturnsToken(){
        UserRequestTransport userRequestTransport = new UserRequestTransport("Qendrim","123");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(userRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/authenticate/login", json);
            ResponseMessageTransport responseMessageTransport = objectMapper.readValue(response, ResponseMessageTransport.class);
            assertEquals("1", responseMessageTransport.getResponseMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = ChatAppException.class)
    public void loginUser_withUserRequestTransport_throwsUserNotFoundError(){
        UserRequestTransport userRequestTransport = new UserRequestTransport("test","");
        String json = null;
        try {
            json = objectMapper.writeValueAsString(userRequestTransport);
            String response = RequestHandler.sendJsonPostRequest("http://localhost:8080/authenticate/login", json);
            assertEquals(any(), response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
