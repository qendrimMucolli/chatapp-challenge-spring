package challenge.chat.util;

public class UtilInfo {

    private static String token;
    private static int roomId;
    private static long lastRequestTime;

    public static int getRoomId() {
        return roomId;
    }

    public static void setRoomId(int roomId) {
        UtilInfo.roomId = roomId;
    }

    public static void clearRoom(){
        UtilInfo.roomId = 0;
    }

    public static void setToken(String token){
        UtilInfo.token = token;
    }

    public static String getToken(){
        return token;
    }

    public static void clearToken(){
        token = null;
    }

    public static long getLastRequestTime() {
        return lastRequestTime;
    }

    public static void setLastRequestTime(long lastRequestTime) {
        UtilInfo.lastRequestTime = lastRequestTime;
    }
}
