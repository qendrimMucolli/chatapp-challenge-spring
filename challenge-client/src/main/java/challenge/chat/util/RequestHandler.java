package challenge.chat.util;

import challenge.chat.dto.response.UserGetNewMessagesResponseTransport;
import org.springframework.http.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class RequestHandler {

    private static RestTemplate restTemplate;
    private static HttpHeaders headers;
    private static String token;

    public static String sendJsonPostRequest(String url, String jsonObject) {
        try {
            refreshTemplateAndHeaders();
            setHeaderForTokenAndApplicationJson();

            HttpEntity<String> entity = new HttpEntity<>(jsonObject, headers);
            return restTemplate.postForObject(url, entity, String.class);
        }catch (HttpServerErrorException.InternalServerError exception){
            throw new ChatAppException("Request could not be sent");
        }
    }

    public static String sendJsonAndParameterPostRequest(String url, Map<String, String> parameter) {
        try {
            refreshTemplateAndHeaders();
            setHeaderForTokenAndApplicationJson();

            HttpEntity<String> entity = new HttpEntity<>(headers);
            return restTemplate.postForObject(url, entity, String.class, parameter);
        }catch (HttpServerErrorException.InternalServerError exception){
            throw new ChatAppException("Request could not be sent");
        }
    }

    public static String sendGetRequest(String url) {
        try {
            refreshTemplateAndHeaders();
            setHeaderForTokenAndApplicationJson();

            HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
            ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
            return result.getBody();
        }catch (HttpServerErrorException.InternalServerError exception){
            throw new ChatAppException("Request could not be sent");
        }
    }

    public static ResponseEntity<UserGetNewMessagesResponseTransport> sendGetRequestForMessages() {
        try {
            refreshTemplateAndHeaders();
            setHeaderForTokenAndApplicationJson();

            String url = "http://localhost:8080/message/{lastFetchTime}";
            Map<String, String> params = new HashMap<>();
            params.put("lastFetchTime", UtilInfo.getLastRequestTime() + "");


            HttpEntity entity = new HttpEntity(headers);
            ResponseEntity<UserGetNewMessagesResponseTransport> response = restTemplate.exchange(url, HttpMethod.GET, entity, UserGetNewMessagesResponseTransport.class, params);
            UtilInfo.setLastRequestTime(System.currentTimeMillis());
            return response;
        }catch (HttpServerErrorException.InternalServerError exception){
            throw new ChatAppException("Request could not be sent");
        }
    }

    public static void refreshTemplateAndHeaders() {
        restTemplate = new RestTemplate();
        headers = new HttpHeaders();
    }

    public static void setHeaderForTokenAndApplicationJson() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);
    }

    public static void setToken(String token) {
        RequestHandler.token = token;
    }

    public static String getToken() {
        return token;
    }
}
