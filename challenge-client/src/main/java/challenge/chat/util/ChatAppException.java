package challenge.chat.util;

public class ChatAppException extends RuntimeException {

    public ChatAppException(String msg) {
        super(msg);
    }
}
