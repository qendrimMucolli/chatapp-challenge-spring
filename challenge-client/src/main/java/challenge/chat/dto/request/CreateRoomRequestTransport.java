package challenge.chat.dto.request;

public class CreateRoomRequestTransport {

    private String roomName;

    public CreateRoomRequestTransport() {
    }

    public CreateRoomRequestTransport(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
