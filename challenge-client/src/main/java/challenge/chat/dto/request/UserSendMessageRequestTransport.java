package challenge.chat.dto.request;

public class UserSendMessageRequestTransport {

    private String messageContent;

    public UserSendMessageRequestTransport() {
    }

    public UserSendMessageRequestTransport(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
}
