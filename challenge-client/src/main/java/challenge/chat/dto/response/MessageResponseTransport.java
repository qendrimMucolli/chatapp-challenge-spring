package challenge.chat.dto.response;

public class MessageResponseTransport {

    private int messageId;
    private String messageSender;
    private String messageContent;

    public MessageResponseTransport() {
    }

    public MessageResponseTransport(int messageId, String messageSender, String messageContent) {
        this.messageId = messageId;
        this.messageSender = messageSender;
        this.messageContent = messageContent;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
}
