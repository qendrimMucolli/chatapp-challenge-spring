package challenge.chat.dto.response;

import java.util.List;

public class UserGetNewMessagesResponseTransport {

    private List<MessageResponseTransport> messages;

    public UserGetNewMessagesResponseTransport() {
    }

    public UserGetNewMessagesResponseTransport(List<MessageResponseTransport> messages) {
        this.messages = messages;
    }

    public List<MessageResponseTransport> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageResponseTransport> messages) {
        this.messages = messages;
    }
}
