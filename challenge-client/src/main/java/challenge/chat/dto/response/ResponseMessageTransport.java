package challenge.chat.dto.response;

public class ResponseMessageTransport {

    private String responseMessage;

    public ResponseMessageTransport() {
    }

    public ResponseMessageTransport(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
