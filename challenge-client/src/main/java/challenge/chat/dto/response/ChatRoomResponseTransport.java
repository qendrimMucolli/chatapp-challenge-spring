package challenge.chat.dto.response;

public class ChatRoomResponseTransport {

    private int roomId;
    private String roomName;

    public ChatRoomResponseTransport() {
    }

    public ChatRoomResponseTransport(int roomId, String roomName) {
        this.roomId = roomId;
        this.roomName = roomName;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
