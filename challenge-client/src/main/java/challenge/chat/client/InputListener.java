package challenge.chat.client;

import challenge.chat.dto.request.CreateRoomRequestTransport;
import challenge.chat.dto.request.UserRequestTransport;
import challenge.chat.dto.request.UserSendMessageRequestTransport;
import challenge.chat.dto.response.*;
import challenge.chat.util.UtilInfo;
import challenge.chat.util.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class InputListener implements Runnable {

    private Scanner scanner;
    private boolean havingConversation;
    private ObjectMapper objectMapper;

    InputListener() {
        scanner = new Scanner(System.in);
    }

    @Override
    public void run() {
        objectMapper = new ObjectMapper();
        String userInput;
        try {
            do {
                userInput = scanner.nextLine();
                if (isRequest(userInput)) {
                    if (!havingConversation || userInput.equals("/leave")) {
                        if (userInput.matches("/joinRoom " + ".*\\d.*")) {
                            joinRoom(userInput);
                            havingConversation = true;
                        } else if (userInput.equals("/leave")) {
                            leaveRoom();
                            havingConversation = false;
                        } else if (userInput.equals("/listRooms")) {
                            listRooms();
                        } else if (userInput.startsWith("/createRoom ")) {
                            createRoom(userInput);
                        } else if (userInput.startsWith("/register ")) {
                            register(userInput);
                            System.out.println();
                            System.out.println("Use /login \"username\" and \"password\" to continue");
                        } else if (userInput.startsWith("/login"))
                            login(userInput);
                    } else {
                        System.out.println("You have to leave the room first");
                    }
                } else {
                    sendMessage(userInput);
                }
            } while (!userInput.equals("/logout"));
            System.exit(0);
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }

    private void register(String httpMessage) throws IOException {
        String name = httpMessage.split(" ")[1];
        String password = httpMessage.split(" ")[2];
        String url = "http://localhost:8080/authenticate/";
        UserRequestTransport registerUserRequestTransport = new UserRequestTransport(name, password);
        String jsonObject = objectMapper.writeValueAsString(registerUserRequestTransport);
        RequestHandler.sendJsonPostRequest(url, jsonObject);
        System.out.println();
        System.out.println("You have been registered successfully!");
    }

    private void login(String request) throws IOException {
        String url = "http://localhost:8080/authenticate/login";
        String name = request.split(" ")[1];
        String password = request.split(" ")[2];
        UserRequestTransport loginUserRequestTransport = new UserRequestTransport(name, password);
        String jsonObject = objectMapper.writeValueAsString(loginUserRequestTransport);
        String response = RequestHandler.sendJsonPostRequest(url, jsonObject);
        ResponseMessageTransport id = objectMapper.readValue(response, ResponseMessageTransport.class);
        RequestHandler.setToken(id.getResponseMessage());
        UtilInfo.setToken(RequestHandler.getToken());
        System.out.println("Login was successful!");
        System.out.println();
        printStartingMessage();
    }

    private void createRoom(String httpMessage) throws IOException {
        String[] temp = httpMessage.split(" ");
        String name = temp[1];
        String url = "http://localhost:8080/room/";
        CreateRoomRequestTransport createRoomRequestTransport = new CreateRoomRequestTransport(name);
        String jsonObject = objectMapper.writeValueAsString(createRoomRequestTransport);
        RequestHandler.sendJsonPostRequest(url, jsonObject);
        System.out.println();
        System.out.println("Room created successfully!");
        System.out.println("-------------------------");
    }

    private void listRooms() {
        String url = "http://localhost:8080/room/";
        System.out.println("List of available rooms: " + RequestHandler.sendGetRequest(url));
    }

    private void joinRoom(String httpMessage) throws IOException {
        String id = httpMessage.split(" ")[1];
        Map<String, String> params = new HashMap<>();
        params.put("id", id);

        String url = "http://localhost:8080/room/{id}/join";
        String response = RequestHandler.sendJsonAndParameterPostRequest(url, params);
        ChatRoomResponseTransport chatRoomResponseTransport = objectMapper.readValue(response, ChatRoomResponseTransport.class);
        UtilInfo.setRoomId(chatRoomResponseTransport.getRoomId());
        System.out.println();
        System.out.println("Room joined successfully!");
        System.out.println("-------------------------");
    }

    private void sendMessage(String userInput) throws IOException {
        String url = "http://localhost:8080/message/";
        UserSendMessageRequestTransport messageRequestDTO = new UserSendMessageRequestTransport(userInput);
        String jsonObject = objectMapper.writeValueAsString(messageRequestDTO);
        RequestHandler.sendJsonPostRequest(url, jsonObject);
    }

    private void leaveRoom() throws IOException {
        String url = "http://localhost:8080/room/{id}/leave";
        String responseRoom = RequestHandler.sendGetRequest("http://localhost:8080/room/user");
        GetRoomOfUserResponseTransport roomOfUserResponseDTO = objectMapper.readValue(responseRoom, GetRoomOfUserResponseTransport.class);

        Map<String, String> params = new HashMap<>();
        params.put("id", roomOfUserResponseDTO.getRoomId() + "");

        RequestHandler.sendJsonAndParameterPostRequest(url, params);
        UtilInfo.clearRoom();
        UtilInfo.setLastRequestTime(0);
        System.out.println("You have left the chat room.");
    }

    private boolean isRequest(String request) {
        return request.startsWith("/");
    }

    private void printStartingMessage() {
        System.out.println();
        System.out.println("------------------------------");
        System.out.println("List of usable commands:");
        System.out.println("/createRoom \"id\" \"name\"");
        System.out.println("/joinRoom \"id\"");
        System.out.println("/listRooms all");
        System.out.println("/logout");
        System.out.println("------------------------------");
    }
}
