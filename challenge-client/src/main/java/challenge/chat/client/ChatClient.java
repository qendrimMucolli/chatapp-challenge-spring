package challenge.chat.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ChatClient {

    private static void startListeningToRequests() {
        System.out.println();
        System.out.println("Use /register or /login \"username\" \"password\" to start the chat app");
        new Thread(new InputListener()).start();
    }

    public static void main(String[] args) {
        SpringApplication.run(ChatClient.class);
        ChatClient.startListeningToRequests();
    }

}
