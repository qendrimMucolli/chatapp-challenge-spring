package challenge.chat.client;

import challenge.chat.dto.response.MessageResponseTransport;
import challenge.chat.dto.response.UserGetNewMessagesResponseTransport;
import challenge.chat.util.RequestHandler;
import challenge.chat.util.UtilInfo;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChatReader {

    @Scheduled(fixedRate = 1000)
    public void getMessage() {
        if (UtilInfo.getToken() != null && UtilInfo.getRoomId() != 0) {
            ResponseEntity<UserGetNewMessagesResponseTransport> response = RequestHandler.sendGetRequestForMessages();
            List<MessageResponseTransport> messages = response.getBody().getMessages();
            for (MessageResponseTransport message : messages) {
                System.out.println(message.getMessageSender() + ": " + message.getMessageContent());
            }
        }
    }
}
