DROP DATABASE IF EXISTS chat_app-db;
use chatapp-db;

DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS chat_room;

CREATE TABLE chat_room (
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    PRIMARY KEY(id)
)  AUTO_INCREMENT = 1 ENGINE=INNODB;

CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    name varchar(45),
    password varchar(45),
    chat_room_id INT NULL,
    PRIMARY KEY(id),
    INDEX (chat_room_id),
    FOREIGN KEY (chat_room_id)
    REFERENCES chat_room(id)
)  AUTO_INCREMENT = 1 ENGINE=INNODB;


CREATE TABLE message(
    id INT NOT NULL AUTO_INCREMENT,
    content varchar(250) NULL,
    sent_time TIMESTAMP,
    chat_room_id INT NULL,
    user_id INT NULL,
    PRIMARY KEY (id),
    INDEX (chat_room_id),
    FOREIGN KEY (chat_room_id)
    REFERENCES chat_room(id)
) AUTO_INCREMENT = 1 ENGINE=INNODB;