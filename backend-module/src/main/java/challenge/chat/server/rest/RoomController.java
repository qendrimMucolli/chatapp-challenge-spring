package challenge.chat.server.rest;

import challenge.chat.server.dto.request.CreateRoomRequestTransport;
import challenge.chat.server.dto.response.*;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.User;
import challenge.chat.server.service.RoomService;
import challenge.chat.server.service.UserService;
import challenge.chat.server.util.ChatAppException;
import challenge.chat.server.util.IdStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/room")
public class RoomController {

    private RoomService roomService;
    private UserService userService;

    public RoomController(RoomService roomService, UserService userService) {
        this.roomService = roomService;
        this.userService = userService;
    }

    @PostMapping("/")
    public ResponseMessageTransport createRoom(@RequestBody CreateRoomRequestTransport createRoomRequestTransport) {
        if(testIfRoomNameIsEmpty(createRoomRequestTransport.getRoomName()))
            throw new ChatAppException("No given room name");
        ChatRoom chatRoom = new ChatRoom(createRoomRequestTransport.getRoomName());
        roomService.createRoom(chatRoom);
        return new ResponseMessageTransport("Chat room was created successfully!");
    }

    @PostMapping("/{id}/join")
    public ChatRoomResponseTransport joinRoom(@PathVariable int id) {
        ChatRoom chatRoom = roomService.findChatRoomById(id);
        userService.joinRoom(chatRoom);
        return new ChatRoomResponseTransport(chatRoom.getId(), chatRoom.getName());
    }

    @PostMapping("/{id}/leave")
    public ResponseMessageTransport leaveRoom(@PathVariable int id) {
        return userService.leaveRoom(id);
    }

    @GetMapping("/")
    public List<ChatRoomResponseTransport> listRooms() {
        return roomService.listRooms();
    }

    @GetMapping("/user")
    public GetRoomOfUserResponseTransport getRoomOfUser() {
        User user = userService.getUserByIdStore();
        ChatRoom chatRoom = roomService.findChatRoomByUser(user);
        if(chatRoom == null)
            throw new ChatAppException("Chat room not found");
        return new GetRoomOfUserResponseTransport(chatRoom.getId());
    }

    private boolean testIfRoomNameIsEmpty(String name){
        return name == null || name.trim().isEmpty();
    }
}
