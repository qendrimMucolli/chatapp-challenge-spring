package challenge.chat.server.rest;

import challenge.chat.server.dto.request.UserRequestTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.entity.User;
import challenge.chat.server.service.UserService;
import challenge.chat.server.util.ChatAppException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate")
public class AuthenticationController {

    private UserService userService;

    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/")
    public ResponseMessageTransport register(@RequestBody UserRequestTransport userRequestTransport, BindingResult bindingResult) {
        if(testForValidFields(userRequestTransport.getUsername(), userRequestTransport.getPassword()))
            throw new ChatAppException("Invalid fields!");
        User user = new User(userRequestTransport.getUsername(), userRequestTransport.getPassword());
        userService.register(user);
        return new ResponseMessageTransport("user-registered!");
    }

    @PostMapping("/login")
    public ResponseMessageTransport login(@RequestBody UserRequestTransport user) {
        return userService.loginAndReturnToken(user);
    }

    @PutMapping("/")
    public ResponseMessageTransport update(@RequestBody UserRequestTransport user){
        userService.updateUserInfo(user);
        return new ResponseMessageTransport("User information updated!");
    }


    @DeleteMapping("/{id}")
    public ResponseMessageTransport delete(@PathVariable int id){
        userService.deleteUserById(id);
        return new ResponseMessageTransport("User deleted successfully!");
    }

    private boolean testForValidFields(String username, String password) {
        return (username == null || username.trim().isEmpty()) || (password == null || password.trim().isEmpty());
    }
}
