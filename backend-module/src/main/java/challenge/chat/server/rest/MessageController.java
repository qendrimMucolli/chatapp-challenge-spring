package challenge.chat.server.rest;

import challenge.chat.server.dto.request.UserSendMessageRequestTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.dto.response.UserGetNewMessagesResponseTransport;
import challenge.chat.server.dto.response.UsersWithSeenMessageTransport;
import challenge.chat.server.entity.User;
import challenge.chat.server.service.MessageService;
import challenge.chat.server.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/message")
public class MessageController {

    private MessageService messageService;
    private UserService userService;

    public MessageController(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @PostMapping("/")
    public ResponseMessageTransport sendMessage(@RequestBody UserSendMessageRequestTransport message) {
        User user = userService.getUserByIdStore();
        return messageService.sendMessage(user, message);
    }

    @GetMapping("/{lastFetchTime}")
    public UserGetNewMessagesResponseTransport getMessage(@PathVariable long lastFetchTime) {
        User user = userService.getUserByIdStore();
        return messageService.findRoomMessages(user.getChatRoom(), lastFetchTime);
    }

    @GetMapping("/{id}/seen")
    public UsersWithSeenMessageTransport getSeenMessages(@PathVariable int id){
        return messageService.findSeenMessage(id);
    }

}
