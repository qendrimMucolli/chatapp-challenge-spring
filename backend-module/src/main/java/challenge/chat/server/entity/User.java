package challenge.chat.server.entity;

import challenge.chat.server.util.ChatAppException;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String name;

    @Column
    private String password;

    public List<RoomHistory> getRoomStates() {
        return roomStates;
    }

    public void setRoomStates(List<RoomHistory> roomStates) {
        this.roomStates = roomStates;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "chat_room_id")
    private ChatRoom chatRoom;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade = CascadeType.ALL)
    private List<RoomHistory> roomStates;

    @Transient
    private Timestamp lastRequestTime;

    public User() {
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ChatRoom getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this.chatRoom = chatRoom;
    }

    public void addRoomHistoryToList(RoomHistory roomHistory){
        if(roomHistory == null) {
            throw new ChatAppException("Room history cannot be null");
        }
        if(testIfRoomHistoryExists(roomHistory)) {
            RoomHistory roomHistory1 = getRoomHistoryIfExists(roomHistory);
            roomHistory1.setStateTime(new Timestamp(System.currentTimeMillis()));
            roomStates.add(roomHistory1);
            return;
        }
        roomStates.add(roomHistory);
    }

    private RoomHistory getRoomHistoryIfExists(RoomHistory roomHistory){
        for (RoomHistory roomHistory1: roomStates){
            if(roomHistory1.equals(roomHistory))
                return roomHistory1;
        }
        return null;
    }

    private boolean testIfRoomHistoryExists(RoomHistory roomHistory){
        return getRoomHistoryIfExists(roomHistory) != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }
}

