package challenge.chat.server.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "state")
public class RoomHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    private ChatRoom room;

    @Column
    @CreationTimestamp
    private Timestamp stateTime;

    public RoomHistory(){

    }

    public RoomHistory(User user, ChatRoom room) {
        this.user = user;
        this.room = room;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ChatRoom getRoom() {
        return room;
    }

    public void setRoom(ChatRoom room) {
        this.room = room;
    }

    public Timestamp getStateTime() {
        return stateTime;
    }

    public void setStateTime(Timestamp stateTime) {
        this.stateTime = stateTime;
    }

    @Override
    public boolean equals(Object object){
        if(object instanceof RoomHistory){
            RoomHistory roomHistory = (RoomHistory)object;
            return roomHistory.getRoom().equals(room)
                    && roomHistory.getUser().equals(user);
        }
        return false;
    }
}
