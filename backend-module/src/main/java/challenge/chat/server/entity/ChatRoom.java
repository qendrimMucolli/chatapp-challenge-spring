package challenge.chat.server.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "chat_room")
public class ChatRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chatRoom")
    private List<User> users;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chatRoom")
    private List<Message> messages;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "room",cascade = CascadeType.ALL)
    private List<RoomHistory> roomStates;

    public ChatRoom() {
    }

    public void addMessage(Message message) {
        messages.add(message);

    }

    public ChatRoom(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }


    @Override
    public String toString() {
        return " " + id + ": " + name + " ";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<RoomHistory> getRoomStates() {
        return roomStates;
    }

    public void setRoomStates(List<RoomHistory> roomStates) {
        this.roomStates = roomStates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatRoom chatRoom = (ChatRoom) o;
        return id == chatRoom.id;
    }

}
