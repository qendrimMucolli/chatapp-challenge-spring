package challenge.chat.server.util;


public class IdStore {

    private static ThreadLocal<Integer> userId = new ThreadLocal<Integer>();

    public static void setUserId(int id){
        userId.set(id);
    }

    public static Integer getUserId(){
        return userId.get();
    }

    public static void clearId(){
        userId.remove();
    }
}
