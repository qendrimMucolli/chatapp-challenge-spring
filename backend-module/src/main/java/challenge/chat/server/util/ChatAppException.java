package challenge.chat.server.util;

public class ChatAppException extends RuntimeException {

    public ChatAppException(String msg) {
        super(msg);
    }
}
