package challenge.chat.server.util;

import challenge.chat.server.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationContext extends OncePerRequestFilter {

    private UserService userService;

    public AuthenticationContext(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        String authorization = httpServletRequest.getHeader("Authorization");
        String uri = httpServletRequest.getRequestURI();

        if (uri.equals("/authenticate/") || uri.equals("/authenticate/login")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        try {
            String id = authorization.substring(authorization.lastIndexOf(" ") + 1);
            if (userService.getUserByToken(id) != null) {
                IdStore.setUserId(Integer.parseInt(id));
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            } else
                httpServletResponse.sendError(403);
        } finally {
            IdStore.clearId();
        }
    }
}
