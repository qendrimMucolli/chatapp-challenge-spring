package challenge.chat.server.repository;

import challenge.chat.server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "SELECT * FROM user u WHERE u.name = :username AND u.password = :password", nativeQuery = true)
    User getLoggedUser(String username, String password);

}
