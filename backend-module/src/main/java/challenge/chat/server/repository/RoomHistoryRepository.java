package challenge.chat.server.repository;

import challenge.chat.server.entity.RoomHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomHistoryRepository extends CrudRepository<RoomHistory, Integer> {

    @Query(value = "SELECT * FROM state s WHERE s.user_id = ?1 AND s.room_id=?2", nativeQuery = true)
    RoomHistory findHistoryByUserAndRoom(int user_id, int room_id);
}
