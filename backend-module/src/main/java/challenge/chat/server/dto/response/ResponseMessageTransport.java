package challenge.chat.server.dto.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseMessageTransport {

    private String responseMessage;

    public ResponseMessageTransport() {
    }

    public ResponseMessageTransport(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public void setJsonObjectAsMessage(Object objectAsMessage){
        try {
            String json = new ObjectMapper().writeValueAsString(objectAsMessage);
            setResponseMessage(json);
        }catch (JsonProcessingException ex){
            ex.printStackTrace();
        }
    }
}
