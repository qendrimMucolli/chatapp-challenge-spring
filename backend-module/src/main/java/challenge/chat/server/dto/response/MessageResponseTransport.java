package challenge.chat.server.dto.response;

import java.util.ArrayList;
import java.util.List;

public class MessageResponseTransport {

    private int messageId;
    private String messageSender;
    private String messageContent;
    private List<UserResponseTransport> usersThatReadMessage = new ArrayList<>();

    public MessageResponseTransport() {
    }

    public MessageResponseTransport(int messageId, String messageSender, String messageContent) {
        this.messageId = messageId;
        this.messageSender = messageSender;
        this.messageContent = messageContent;
    }

    public MessageResponseTransport(int messageId, String messageSender, String messageContent, List<UserResponseTransport> usersThatReadMessage) {
        this.messageId = messageId;
        this.messageSender = messageSender;
        this.messageContent = messageContent;
        this.usersThatReadMessage = usersThatReadMessage;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public void addUserThatReadTheMessage(UserResponseTransport requestTransport){
        usersThatReadMessage.add(requestTransport);
    }

    public List<UserResponseTransport> getUsersThatReadMessage() {
        return usersThatReadMessage;
    }

    public void setUsersThatReadMessage(List<UserResponseTransport> usersThatReadMessage) {
        this.usersThatReadMessage = usersThatReadMessage;
    }
}
