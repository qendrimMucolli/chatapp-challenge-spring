package challenge.chat.server.dto.response;

public class LoginUserResponseTransport {

    private String token;

    public LoginUserResponseTransport() {
    }

    public LoginUserResponseTransport(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
