package challenge.chat.server.dto.response;

import challenge.chat.server.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UsersWithSeenMessageTransport {

    private List<UserResponseTransport> users;
    private MessageResponseTransport  message;

    public UsersWithSeenMessageTransport() {
        users = new ArrayList<>();
    }

    public UsersWithSeenMessageTransport(List<UserResponseTransport> user, MessageResponseTransport message) {
        this.users = user;
        this.message = message;
    }

    public List<UserResponseTransport> getUser() {
        return users;
    }

    public void setUser(List<UserResponseTransport> user) {
        this.users = user;
    }

    public MessageResponseTransport getMessage() {
        return message;
    }

    public void setMessage(MessageResponseTransport message) {
        this.message = message;
    }

    public void addUser(UserResponseTransport user){
        users.add(user);
    }
}
