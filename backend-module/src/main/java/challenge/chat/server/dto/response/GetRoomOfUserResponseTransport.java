package challenge.chat.server.dto.response;

public class GetRoomOfUserResponseTransport {

    private int roomId;

    public GetRoomOfUserResponseTransport() {
    }

    public GetRoomOfUserResponseTransport(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
}
