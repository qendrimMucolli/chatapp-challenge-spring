package challenge.chat.server.dto.response;

import challenge.chat.server.entity.ChatRoom;

public class UserResponseTransport {

    private int id;
    private String name;
    private ChatRoomResponseTransport room;

    public UserResponseTransport() {
    }

    public UserResponseTransport(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserResponseTransport(int id, String name, ChatRoomResponseTransport room) {
        this.id = id;
        this.name = name;
        this.room = room;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatRoomResponseTransport getRoom() {
        return room;
    }

    public void setRoom(ChatRoomResponseTransport room) {
        this.room = room;
    }
}
