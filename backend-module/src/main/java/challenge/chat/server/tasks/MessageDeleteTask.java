package challenge.chat.server.tasks;

import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.Message;
import challenge.chat.server.entity.RoomHistory;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.ChatRoomRepository;
import challenge.chat.server.repository.MessageRepository;
import challenge.chat.server.repository.RoomHistoryRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageDeleteTask {

    private ChatRoomRepository roomRepository;
    private MessageRepository messageRepository;
    private RoomHistoryRepository roomHistoryRepository;

    public MessageDeleteTask(ChatRoomRepository roomRepository,
                             MessageRepository messageRepository,
                             RoomHistoryRepository roomHistoryRepository) {
        this.roomRepository = roomRepository;
        this.messageRepository = messageRepository;
        this.roomHistoryRepository = roomHistoryRepository;
    }

    @Scheduled(fixedRateString = "${MESSAGE.DELETE.POLLING.RATE}")
    public void deleteSeenMessages() {
        List<ChatRoom> rooms = roomRepository.findAll();
        for (ChatRoom room : rooms) {
            deleteSeenMessages(room.getMessages());
        }
    }

    private void deleteSeenMessages(List<Message> messages) {
        for (Message message : messages) {
            if (canDeleteMessage(message, message.getChatRoom()))
                messageRepository.delete(message);
        }
    }

    private boolean canDeleteMessage(Message message, ChatRoom room) {
        for (RoomHistory roomHistory : room.getRoomStates()) {
            if(!canDeleteMessage(roomHistory.getUser(), message)) {
                return false;
            }
        }
        return true;
    }

    private boolean canDeleteMessage(User user, Message message){
        RoomHistory roomHistory = roomHistoryRepository.findHistoryByUserAndRoom(user.getId(), message.getChatRoom().getId());
        return user.getChatRoom().equals(message.getChatRoom()) || message.getSent_time().after(roomHistory.getStateTime());
    }
}
