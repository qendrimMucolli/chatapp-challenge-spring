package challenge.chat.server.service;

import challenge.chat.server.dto.response.ChatRoomResponseTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.dto.response.UserResponseTransport;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.ChatRoomRepository;
import challenge.chat.server.util.IdStore;
import challenge.chat.server.util.ChatAppException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private ChatRoomRepository repository;

    public RoomService(ChatRoomRepository repository) {
        this.repository = repository;
    }

    public ResponseMessageTransport createRoom(ChatRoom chatRoom) {
        if (IdStore.getUserId() == null)
            throw new ChatAppException("You must be logged in to use this feature.");
        repository.save(chatRoom);
        return new ResponseMessageTransport("Chat room created!");
    }

    public ChatRoom findChatRoomById(int id) {
        Optional<ChatRoom> roomOptional =  repository.findById(id);
        ChatRoom room = roomOptional.orElse(null);
        if (room == null)
            throw new ChatAppException("Chat room not found");
        return room;
    }

    public List<ChatRoomResponseTransport> listRooms() {
        if (IdStore.getUserId() == null)
            throw new ChatAppException("You must be logged in to use this feature.");
        return getConvertedChatRoomsToDTO();
    }

    public ChatRoom findChatRoomByUser(User user) {
        return user.getChatRoom();
    }

    private List<ChatRoomResponseTransport> getConvertedChatRoomsToDTO() throws ChatAppException{
        List<ChatRoom> chatRooms = repository.findAll();
        if (chatRooms == null)
            throw new ChatAppException("There are no rooms available");

        List<ChatRoomResponseTransport> chatRoomResponseTransports = new ArrayList<>();

        for (ChatRoom chatRoom : chatRooms) {
            chatRoomResponseTransports.add(new ChatRoomResponseTransport(chatRoom.getId(), chatRoom.getName()));
        }
        return chatRoomResponseTransports;
    }
}
