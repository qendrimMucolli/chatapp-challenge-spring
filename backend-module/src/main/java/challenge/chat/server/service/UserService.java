package challenge.chat.server.service;

import challenge.chat.server.dto.request.UserRequestTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.RoomHistory;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.UserRepository;
import challenge.chat.server.util.ChatAppException;
import challenge.chat.server.util.IdStore;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public UserRequestTransport register(User user) {
        repository.save(user);
        if (user.getName() == null || user.getPassword() == null)
            throw new ChatAppException("Invalid input fields");
        return new UserRequestTransport(user.getName(), user.getPassword());
    }

    public ResponseMessageTransport joinRoom(ChatRoom chatRoom) {
        if (IdStore.getUserId() == null)
            throw new ChatAppException("You must be logged in to use this feature.");
        User user = getUserByToken(IdStore.getUserId().toString());
        if (user == null)
            throw new ChatAppException("User not found");

        RoomHistory roomHistory = new RoomHistory(user, chatRoom);
        user.setChatRoom(chatRoom);
        user.addRoomHistoryToList(roomHistory);
        repository.save(user);
        return new ResponseMessageTransport("Joined room: " + chatRoom.getId());
    }

    public ResponseMessageTransport leaveRoom(int id) {
        if (IdStore.getUserId() == null)
            throw new ChatAppException("You must be logged in to use this feature.");
        if (id == 0)
            throw new ChatAppException("Chat room not found");

        Optional<User> userOptional = repository.findById(IdStore.getUserId());
        User user = getUserFromOptional(userOptional);
        RoomHistory roomHistory = new RoomHistory(user, user.getChatRoom());
        user.setChatRoom(null);
        user.addRoomHistoryToList(roomHistory);
        repository.save(user);
        return new ResponseMessageTransport("Left room with id " + id);
    }

    public ResponseMessageTransport loginAndReturnToken(UserRequestTransport loginUserRequestTransport) {
        User userTemp = repository.getLoggedUser(loginUserRequestTransport.getUsername(), loginUserRequestTransport.getPassword());
        if (userTemp == null)
            throw new ChatAppException("User not found");
        return new ResponseMessageTransport(userTemp.getId() + "");
    }

    public User getUserByToken(String id) {
        Optional<User> user = repository.findById(Integer.parseInt(id));
        return user.orElse(null);
    }

    public User getUserByIdStore() {
        if (IdStore.getUserId() == null)
            throw new ChatAppException("You must be logged in to use this feature.");
        Optional<User> userOptional = repository.findById(IdStore.getUserId());
        return getUserFromOptional(userOptional);
    }

    public ResponseMessageTransport updateUserInfo(UserRequestTransport userRequestTransport) {
        if (userRequestTransport == null)
            throw new ChatAppException("User not found");

        Optional<User> userOptional = repository.findById(userRequestTransport.getId());
        User user = getUserFromOptional(userOptional);
        user.setName(userRequestTransport.getUsername());
        user.setPassword(userRequestTransport.getPassword());
        repository.save(user);
        return new ResponseMessageTransport("User information updated!");
    }

    public ResponseMessageTransport deleteUserById(int id) {
        if (id == 0)
            throw new ChatAppException("Invalid id");
        repository.deleteById(id);
        return new ResponseMessageTransport("User deleted successfully!");
    }

    private User getUserFromOptional(Optional<User> userOptional) {
        User user = userOptional.orElse(null);
        if (user == null)
            throw new NullPointerException("User could not be found!");

        return user;
    }
}
