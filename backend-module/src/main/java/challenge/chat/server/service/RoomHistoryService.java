package challenge.chat.server.service;

import challenge.chat.server.entity.RoomHistory;
import challenge.chat.server.repository.RoomHistoryRepository;
import challenge.chat.server.util.ChatAppException;
import org.springframework.stereotype.Service;

@Service
public class RoomHistoryService{

    private RoomHistoryRepository repository;

    public RoomHistoryService(RoomHistoryRepository roomHistoryRepository){
        repository = roomHistoryRepository;
    }

    public RoomHistory saveRoomHistory(RoomHistory roomHistory){
        if(roomHistory == null)
            throw new ChatAppException("Room History cannot be null");
        return repository.save(roomHistory);
    }
}
