package challenge.chat.server.service;

import challenge.chat.server.dto.request.UserSendMessageRequestTransport;
import challenge.chat.server.dto.response.*;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.Message;
import challenge.chat.server.entity.RoomHistory;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.MessageRepository;
import challenge.chat.server.repository.RoomHistoryRepository;
import challenge.chat.server.util.ChatAppException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    private MessageRepository messageRepository;
    private RoomHistoryRepository roomHistoryRepository;

    public MessageService(MessageRepository messageRepository, RoomHistoryRepository roomHistoryRepository) {
        this.messageRepository = messageRepository;
        this.roomHistoryRepository = roomHistoryRepository;
    }

    public ResponseMessageTransport sendMessage(User user, UserSendMessageRequestTransport userMessageDTO) {
        if (user == null)
            throw new ChatAppException("User must be instantiated");

        Message message = new Message(userMessageDTO.getMessageContent());
        message.setUser(user);
        message.setChatRoom(user.getChatRoom());
        messageRepository.save(message);
        return new ResponseMessageTransport("Message was sent successfully!");
    }

    public UserGetNewMessagesResponseTransport findRoomMessages(ChatRoom chatRoom, long lastFetchTime) {
        if (chatRoom == null)
            throw new ChatAppException("Chat room must be instantiated");

        Timestamp timestamp = new Timestamp(lastFetchTime);
        UserGetNewMessagesResponseTransport userGetNewMessagesResponseTransport = new UserGetNewMessagesResponseTransport();
        List<Message> messages = chatRoom.getMessages().stream().filter(message -> message.getSent_time().after(timestamp))
                .sorted((message, t1) -> message.compareTo(t1.getSent_time())).collect(Collectors.toList());
        List<MessageResponseTransport> convertedMessages = getConvertedListOfMessagesToMessageDTO(messages);
        userGetNewMessagesResponseTransport.setMessages(convertedMessages);
        return userGetNewMessagesResponseTransport;
    }

    private List<MessageResponseTransport> getConvertedListOfMessagesToMessageDTO(List<Message> messages) {
        int startMessagePosition = 0;
        int lastMessagePosition = 0;
        List<MessageResponseTransport> messageResponseTransports = messages.stream().map
                (message -> new MessageResponseTransport(message.getId(), message.getContent(), message.getUser().getName(), getUsersThatSawTheMessage(message)))
                .limit(15).collect(Collectors.toList());

        if (messages.size() > 15) {
            startMessagePosition = messages.size() - 16;
            lastMessagePosition = messages.size() - 1;
            return messageResponseTransports.subList(startMessagePosition, lastMessagePosition);
        }
        return messageResponseTransports;
    }

    private List<UserResponseTransport> getUsersThatSawTheMessage(Message message) {
        List<UserResponseTransport> userResponseTransports = new ArrayList<>();
        for (RoomHistory roomHistory : message.getChatRoom().getRoomStates()) {
            User user = roomHistory.getUser();
            if(testIfMessageIsSeen(roomHistory.getUser(), message)) {
                userResponseTransports.add(new UserResponseTransport(user.getId(), user.getName()));
            }
        }
        return userResponseTransports;
    }

    private boolean testIfMessageIsSeen(User user, Message message){
        RoomHistory roomHistory = roomHistoryRepository.findHistoryByUserAndRoom(user.getId(), message.getChatRoom().getId());
        return user.getChatRoom().equals(message.getChatRoom()) || message.getSent_time().after(roomHistory.getStateTime());
    }

    public UsersWithSeenMessageTransport findSeenMessage(int id) {
        Message message = messageRepository.findById(id).orElse(null);
        User user = message.getUser();
        ChatRoom room = message.getChatRoom();
        RoomHistory roomHistory = roomHistoryRepository.findHistoryByUserAndRoom(user.getId(), room.getId());
        return findUsersWithSeenMessage(message, room, roomHistory);
    }

    private UsersWithSeenMessageTransport findUsersWithSeenMessage(Message message, ChatRoom room,RoomHistory roomHistory) {
        UsersWithSeenMessageTransport usersWithSeenMessageTransport = new UsersWithSeenMessageTransport();
        usersWithSeenMessageTransport.setMessage(new MessageResponseTransport(message.getId(), message.getUser().getName(),message.getContent()));
        for (User user: room.getUsers()){
            if(user.getChatRoom().equals(message.getChatRoom()) || message.getSent_time().after(roomHistory.getStateTime())) {
                ChatRoomResponseTransport roomTransport = new ChatRoomResponseTransport(room.getId(), room.getName());
                usersWithSeenMessageTransport.addUser(new UserResponseTransport(user.getId(), user.getName(), roomTransport));
            }
        }
        return usersWithSeenMessageTransport;
    }

}
