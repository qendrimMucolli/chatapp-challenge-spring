package challenge.chat.service;

import challenge.chat.server.dto.request.UserSendMessageRequestTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.Message;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.MessageRepository;
import challenge.chat.server.service.MessageService;
import challenge.chat.server.util.ChatAppException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTests {

    @Mock
    private MessageRepository repository;

    @InjectMocks
    private MessageService messageService;

    @Test
    public void sendMessageToRoom_withUserAndMessageTransport_sendsMessageToRoom(){
        UserSendMessageRequestTransport message = new UserSendMessageRequestTransport("test");
        User user = new User("test","Test");
        ResponseMessageTransport responseMessageTransport = messageService.sendMessage(user,message);
        assertEquals("Message was sent successfully!", responseMessageTransport.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void sendMessageToRoom_withUserAndMessageTransport_throwsUserNullError(){
        UserSendMessageRequestTransport message = new UserSendMessageRequestTransport("test");
        User user = null;
        ResponseMessageTransport responseMessageTransport = messageService.sendMessage(user,message);
        assertEquals("Message was sent successfully!", responseMessageTransport.getResponseMessage());
    }

    @Test
    public void getMessages_withChatRoomAndLastFetchTime_getsMessages(){
        ChatRoom chatRoom = new ChatRoom("test");
        long lastFetchTime = System.currentTimeMillis();
        List<Message> messages = new ArrayList<>();
        createMessagesAndAddToList(messages);
        chatRoom.setMessages(messages);
        messageService.findRoomMessages(chatRoom, lastFetchTime);
    }

    private void createMessagesAndAddToList(List<Message> messages){
        for (int i = 0; i < 4; i++){
            User user = new User("Qendrim" + i, "test");
            Message message = new Message("message " + i);
            message.setId(i+1);
            message.setUser(user);
            message.setSent_time(new Timestamp(System.currentTimeMillis()));
            messages.add(message);
        }
    }

    @Test(expected = ChatAppException.class)
    public void getMessages_withChatRoomAndLastFetchTime_throwsRoomNotFoundError(){
        ChatRoom chatRoom = null;
        long lastFetchTime = System.currentTimeMillis();
        List<Message> messages = new ArrayList<>();
        createMessagesAndAddToList(messages);
        messageService.findRoomMessages(chatRoom, lastFetchTime);
    }
}
