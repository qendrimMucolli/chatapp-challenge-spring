package challenge.chat.service;

import challenge.chat.server.dto.request.UserRequestTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.entity.User;
import challenge.chat.server.repository.UserRepository;
import challenge.chat.server.service.UserService;
import challenge.chat.server.util.IdStore;
import challenge.chat.server.util.ChatAppException;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService userService;


    public UserServiceTests() {
    }

    @After
    public void deleteSetup(){
        IdStore.clearId();
    }

    @Test
    public void createUser_withUsernameAndPassword_createsUser(){
        User user = new User("Qendrim", "testing");
        UserRequestTransport userDTO = userService.register(user);

        assertEquals(user.getName(),userDTO.getUsername());
        assertEquals(user.getPassword(), userDTO.getPassword());
    }

    @Test(expected = ChatAppException.class)
    public void createUser_withUsernameAndPassword_throwsUsernameNullError(){
        User user = new User(null, "testing");
        UserRequestTransport userDTO = userService.register(user);

        assertEquals(user.getName(),userDTO.getUsername());
        assertEquals(user.getPassword(), userDTO.getPassword());
    }

    @Test
    public void getTokenThroughLogin_withUsernameAndPassword_getsUserToken(){
        UserRequestTransport user = new UserRequestTransport("Qendrim", "123");

        when(repository.getLoggedUser("Qendrim", "123")).thenReturn(new User("Qendrim","123"));
        ResponseMessageTransport response = userService.loginAndReturnToken(user);
        assertNotNull(response.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void getTokenThroughLogin_withUsernameAndPassword_throwsUserNotFoundError(){
        UserRequestTransport user = new UserRequestTransport("Qendrim", "123");

        when(repository.getLoggedUser("Qendrim", "123")).thenReturn(null);
        ResponseMessageTransport response = userService.loginAndReturnToken(user);
        assertNotNull(response.getResponseMessage());
    }

    @Test
    public void getUserByToken_withToken_returnsUser(){
        User user = new User("Qendrim", "123");
        String token = "1";

        when(repository.findById(Integer.parseInt(token))).thenReturn(Optional.of(user));
        User userTemp = userService.getUserByToken(token);
        assertEquals(user.getName(), userTemp.getName());
        assertEquals(user.getPassword(), userTemp.getPassword());
    }

    @Test
    public void addUserToRoom_throughChatRoom_addsUserToRoom(){
        ChatRoom room = new ChatRoom("test");
        room.setId(123);
        IdStore.setUserId(4);

        when(repository.findById(4)).thenReturn(Optional.of(new User("Qendrim","123")));

        ResponseMessageTransport responseMessageTransport = userService.joinRoom(room);
        assertEquals("Joined room: " + room.getId(), responseMessageTransport.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void addUserToRoom_throughChatRoom_throwsUserNotFoundError(){
        ChatRoom room = new ChatRoom("test");
        room.setId(123);
        IdStore.setUserId(4);

        when(repository.findById(4)).thenReturn(Optional.empty());

        ResponseMessageTransport responseMessageTransport = userService.joinRoom(room);
        assertEquals("Joined room: " + room.getId(), responseMessageTransport.getResponseMessage());
    }


    @Test
    public void removeUserFromRoom_throughUserId_removesUserFromRoom(){
        IdStore.setUserId(4);
        int id = 4;

        when(repository.findById(4)).thenReturn(Optional.of(new User("Qendrim","123")));
        ResponseMessageTransport responseMessageTransport = userService.leaveRoom(id);
        assertEquals("Left room with id " + id , responseMessageTransport.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void removeUserFromRoom_throughUserId_throwsUserNotFoundError(){
        IdStore.setUserId(4);
        int id = 0;

        ResponseMessageTransport responseMessageTransport = userService.leaveRoom(id);
        assertEquals("Left room with id " + id , responseMessageTransport.getResponseMessage());
    }

    @Test
    public void updateUserInfo_withUsernameAndPassword_updatesUserData(){
        UserRequestTransport requestTransport = new UserRequestTransport("Qendrim", "123");

        when(repository.findById(0)).thenReturn(Optional.of(new User("Qendrim","test")));
        ResponseMessageTransport responseMessageTransport = userService.updateUserInfo(requestTransport);
        assertEquals("User information updated!", responseMessageTransport.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void updateUserInfo_withUsernameAndPassword_throwsUserNotFoundError(){
        UserRequestTransport requestTransport = null;

        ResponseMessageTransport responseMessageTransport = userService.updateUserInfo(requestTransport);
        assertEquals("User information updated!", responseMessageTransport.getResponseMessage());
    }

    @Test
    public void deleteUser_throughUserId_deletesUser(){
        int id = 4;
        ResponseMessageTransport responseMessageTransport = userService.deleteUserById(id);
        assertEquals("User deleted successfully!", responseMessageTransport.getResponseMessage());
    }

    @Test(expected = ChatAppException.class)
    public void deleteUser_throughUserId_throwsUserNotFoundError(){
        int id = 0;
        ResponseMessageTransport responseMessageTransport = userService.deleteUserById(id);
        assertEquals("User deleted successfully!", responseMessageTransport.getResponseMessage());
    }

}
