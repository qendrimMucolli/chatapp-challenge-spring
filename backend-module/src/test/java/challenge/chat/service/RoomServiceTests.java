package challenge.chat.service;

import challenge.chat.server.dto.response.ChatRoomResponseTransport;
import challenge.chat.server.dto.response.ResponseMessageTransport;
import challenge.chat.server.entity.ChatRoom;
import challenge.chat.server.repository.ChatRoomRepository;
import challenge.chat.server.service.RoomService;
import challenge.chat.server.util.ChatAppException;
import challenge.chat.server.util.IdStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class RoomServiceTests {

    @Mock
    private ChatRoomRepository roomRepository;

    @InjectMocks
    private RoomService roomService;

    public RoomServiceTests(){

    }

    @Before
    public void setup(){
        ChatRoom chatRoom = new ChatRoom("test");
        chatRoom.setId(1);
        when(roomRepository.findById(chatRoom.getId())).thenReturn(Optional.of(chatRoom));
    }

    @After
    public void deleteSetup(){
        IdStore.clearId();
    }

    @Test
    public void createRoom_withChatRoom_createsChatRoom(){
        ChatRoom chatRoom = new ChatRoom("test");
        IdStore.setUserId(4);
        ResponseMessageTransport response = roomService.createRoom(chatRoom);
        assertEquals("Chat room created!", response.getResponseMessage());
        IdStore.clearId();
    }

    @Test(expected = ChatAppException.class)
    public void createRoom_withChatRoom_throwsIdStoreNotAssignedError(){
        ChatRoom chatRoom = new ChatRoom("test");
        ResponseMessageTransport response = roomService.createRoom(chatRoom);
        assertEquals("Chat room created!", response.getResponseMessage());
    }

    @Test
    public void getRoom_withId_returnsRoom(){
        ChatRoom room = roomService.findChatRoomById(1);
        assertEquals("test", room.getName());
    }

    @Test(expected = ChatAppException.class)
    public void getRoom_withId_throwsRoomNotFoundError(){
        ChatRoom room = roomService.findChatRoomById(0);
        assertEquals("test", room.getName());
    }

    @Test
    public void getAllRooms_noParameters_returnsListOfRooms(){
        IdStore.setUserId(4);
        List<ChatRoom> rooms = createAndReturnListOfRooms();
        when(roomRepository.findAll()).thenReturn(rooms);
        List<ChatRoomResponseTransport> response = roomService.listRooms();
        utilTestForAssertEquals(rooms, response);
    }

    private List<ChatRoom> createAndReturnListOfRooms(){
        List<ChatRoom> rooms = new ArrayList<>();
        for(int i = 0; i < 4; i++){
            rooms.add(new ChatRoom("room " + i));
        }
        return rooms;
    }

    private void utilTestForAssertEquals(List<ChatRoom> rooms,List<ChatRoomResponseTransport> response){
        for(int i = 0; i < response.size(); i++){
            ChatRoom room = rooms.get(i);
            ChatRoomResponseTransport roomResponse = response.get(i);
            assertEquals(room.getName(), roomResponse.getRoomName());
        }
    }

    @Test(expected = ChatAppException.class)
    public void getAllRooms_noParameters_throwsRoomsNotFoundError(){
        IdStore.setUserId(4);
        when(roomRepository.findAll()).thenReturn(null);
        List<ChatRoomResponseTransport> response = roomService.listRooms();
    }
}
